# AAI training

Welcome to the AAI workshop for Auto Protect Group! In this repo you have all the resources to run through the creation of your own Identity Provider (IdP) and connecting a few example service providers (SPs) to it.

## Getting things running

Firstly you will need to ensure that you have both git and docker installed and running on your laptop:

https://docs.docker.com/get-docker/

https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

Next you should open up some form of terminal and clone this repo:

```
git clone https://gitlab.com/autoprotectgroup/aai-training.git
```

Change directory into the folder that has just been created and then run the containers

```
cd aai-training
docker compose up
```

Give it some time, and if it is successful you should be able to access the Keycloak interface on the following URL: (Security warning - make sure to accept the certificate it presents as it is self-signed PS. Don't use Chrome, **[Firefox is better](https://www.mozilla.org/en-GB/firefox/new/)**)

https://localhost:8443/

## Keycloak Administration

Now you have keycloak running you should be able to login to the administration panel. Click the left hand link on the keycloak dashboard "Administration Console >" and enter the following credentials:

```
username: admin
password: admin
```

Keycloak uses 'realms' to separate different user pools. Each realm can have a different configuration and has completely independently configured users. It also acts as an IdP SP proxy, so you can both connect service providers and use the internal user management features, but also connect upstream identity providers to source your users.

### Adding an upstream Identity Provider

While we can use the in-built user storage of Keycloak demonstrating the power of an IdP SP proxy is really easy. We are going to use GitHub to provide our authentication to Keycloak using the simple guide they have below:

https://docs.github.com/en/developers/apps/building-oauth-apps/creating-an-oauth-app

#### Registering an OAuth service with GitHub

You can register a new OAuth application below:

https://github.com/settings/applications/new

You will need to login or register for a GitHub account. This could be a work account or your personal account, all the data we are using here today will be destroyed when we remove our containers.

Use the following detials when registering your app:

```
Application Name: Keycloak Test
Homepage URL: https://localhost:8443/
Authorization callback URL: https://localhost:8443/auth/realms/master/broker/github/endpoint
```

On the following page you will be able to get the two following key information. Make a note of both of these as you will need them when registering the GitHub provider in Keycloak.

- ClientID
- Client Secret (Click generate a new client secret)

#### Configuring Keycloak with GitHub provider

Now in your Keycloak administration console you will need to add an identity provider. On the left side click "Identity Providers". 

From the drop-down menu select "GitHub" which will take you to a form. For this example all we will configure is the Client ID and Client Secret. Paste the values you got from registering your OAuth application in GitHub here. Click OK and everything is done!

#### Testing your GitHub login

Keycloak has an in-built account management service for users to edit and update their details. The following link is the URL to access the default realm (master) account management page. We can use this to test our GitHub Login.

https://localhost:8443/auth/realms/master/account/#/

Note: You may already be signed in with the admin account, so click "Sign Out" at the top right before continuing.

On the login screen click "Or sign in with: GitHub"

When your browser is redirected to GitHub login with your account (of course this will work for any GitHub account, not just the one you configured the OAuth integration with).

Now you should see an attribute release confirmation form, authorize access to your application and you should be logged in!

If you click "Personal Info" you will see that information about you such as your username, email address and name will have been automatically configured and transferred to Keycloak when logging in.

### Further reading about Keycloak administration

Keycloak has a tonne of additional features as an Identity Provider, such as in-built 2-factor authentication, customisable authentication workflows, support for many other authentication providers and a completely extensible core that can be used to add unsupported authentication sources. Using this you can provide a seamless high-quality migration experience for your users.

Drawbacks for keycloak include the naive API that is for account management, so if you intend to use the local user storage for user data the management tools for users aren't that powerful. The majority of the use cases for Keycloak involve connecting it to on-premise Active Directory/LDAP user storages as a mechanism for surfacing local credentials for organisations into an SSO multi-protocol compliant web authentication server. When twinned with FreeIPA you have a fairly blast-proof enterprise grade authentication infrastructure for both local and online forums using exclusively open-source technologies. Thanks RedHat!

## Connecting a service provider to your IdP

We have users who can authenticate without having to register by directly logging in with GitHub, great! But now we need to actually get some services for the user to access.

Included in this bundle is a React application that is ready to be configured and connected to the Keycloak provider using OIDC.

#### Create a client in Keycloak

Log in to the keycloak administration panel and click Clients on the left hand menu. You'll see a number of clients are setup by default as part of the keycloak installation. Each of these allow components of the keycloak service to connect use authentication, such as the admin interface or the account management tools. We're going to make a new client by clicking the `Create` button at the top right.

You can make the client ID whatever you like, but it's recommended to keep to alpha-numeric characters as this is the ID we will be using in our application to identify which client we are connecting with. The root URL will be used as the basis of all of the Cross-Origin request security. The react app is running on your localhost as port 3000.

```
ClientID: reactapp
Root URL: http://localhost:3000
```

The "Access Type" shown on the page once we have created this is a critical field. It will define whether the client can access using only the client ID or whether a private key will also be used. As React is a front-end technology, we cannot use any private keys to enhance the security and only the origin-URL is used to lock down this kind of access. For this client the default, public, is the right choice.

Before we continue we should change the default client scopes. By default clients will get access to the "profile" and "email" scopes which include data about the user. We should make these optional so that clients that don't need any identifying data about a user can authenticate without receiving them.

Click the Client Scopes tab and remove the `email` and `profile` scopes from the default client scopes. Now add them from the Available scopes on the Optional tab. All changes are saved immediately.

#### Configure ReactJS app

Now we can configure our React App. If you go to the URL of the react app and click login you'll see it isn't working, as we need to configure the OIDC provider.

http://localhost:3000

Configuring an OIDC provider is easy as the spec enforces that a standard configuration information JSON file is available from the authentication server. To get this go to the Realm Settings link and click [OpenID Endpoint Configuration](https://localhost:8443/auth/realms/master/.well-known/openid-configuration). Copy the URL from the address bar.

Edit the config by editing `react-app.env` inside this repo. Add the OIDC config URL and the Client ID that you set when creating the client. Once you've done this you will need to restart the ReactJS container to pick up the changes.

```
docker compose down react && docker compose up react
```

Go to your React application and click login. You should see the keycloak login page, click Sign in with GitHub, login, and you will be redirected back to the react app which will display information about your OIDC token. You will see that by default we only get the `sub` and other standard OIDC attributes. To increase the information we get back we need to change the `scopes` that we request on the authentication.

Scopes can be set inside the `react-app.env` again and are space-delimited, so if you change the line to be as below, restart the container and then login you should find the OIDC token contains much more detail about you!

```
REACT_APP_OAUTH_CLIENT_SCOPE=openid profile email
```

The access token you have inside the ReactJS app could now be added onto future API calls to ensure not just authentication but authorization to different areas of the system as the app grows and the APIs build behind it.

### Further reading on ReactJS and OIDC

The ReactJS app uses a library for managing OIDC **and you should too**. Ensuring consistent token management and regular refreshing of tokens is the majority of user authentication lifecycle within a front-end application, as the role of the authentication is to simply get an active token and use that for subequent calls. There are a lot of pitfalls to hand-cranking things like JWT validation and transparent token refreshing.

Within the app the file `react-oidc-clien-js/src/src/services/AuthService.ts` is responsible for actually handling the authentication. The actual application code written to handle this is very light, and the library is doing most of the heavy lifting. Notice it also relies on two specific HTML files `public/signin-callback.html` and `public/silent-renew.html`, both of which are key at handling the redirect process of OIDC/OAuth2.0 authentication.

https://github.com/IdentityModel/oidc-client-js

## SAML Service Provider

TODO: Add a service provider to demonstrate SAML connection.

## FAQs

#### I get server not found when accessing my Keycloak instance after destroying and recreating the container

The self-signed certificate is re-generated each time the container is rebuilt. Open up your certificate manager and remove the entry for localhost and restart your browser.

## References

React OIDC app based off https://github.com/skoruba/react-oidc-client-js

