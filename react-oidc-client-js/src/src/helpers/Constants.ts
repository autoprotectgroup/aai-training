export class Constants {
  public static stsAuthority = process.env.REACT_APP_OIDC_CONFIG_URL || '';
  public static clientId = process.env.REACT_APP_OAUTH_CLIENT_ID || '';
  public static clientRoot = process.env.REACT_APP_ROOT_URL || 'http://localhost:3000/';
  public static clientScope = process.env.REACT_APP_OAUTH_CLIENT_SCOPE || 'openid';

  public static apiRoot = 'https://demo.identityserver.io/api/';
}
